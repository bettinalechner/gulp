
```
#!bash
> git checkout master~8
> composer global require "laravel/installer=~1.1"
> vim ~/.bashrc
```

At the bottom of the file, add the following, then :wq


```
#!bash

export PATH=~/.composer/vendor/bin:$PATH
```


```
#!bash

> source ~/.bashrc
> npm install
> npm install -g gulp
> composer global require squizlabs/php_codesniffer
> git checkout master~7
> npm install
> gulp
> gulp watch
```