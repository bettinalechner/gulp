class Article {
    constructor() {
        this.title = 'Hello, world!';
    }
    
    sayHello() {
        console.log(this.title);
    }
}

export default Article;