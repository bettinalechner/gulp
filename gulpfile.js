// Import dependencies
var gulp = require('gulp');
var elixir = require('laravel-elixir');
var phpcbf = require('gulp-phpcbf');
var prettify = require('gulp-jsbeautifier');
var gutil = require('gutil');

// Define PHP files to include in PHPCodeSniffer
var php_files = [
    'app/**/*.php',
    'config/**/*.php',
    'tests/**/*.php'
];

// Define PHPCodeSniffer options
var phpcbf_options = {
    bin: 'phpcbf',
    standard: 'PSR2',
    warningSeverity: 0
};

// Define JSBeautifier options
var jsbeautifier_options = {
    mode: 'VERIFY_AND_WRITE',
    js: {
        maxPreserveNewlines: 1
    }
};

elixir.config.registerWatcher('jsbeautify', ['resources/js/**/*.js', '!resources/js/{vendor,vendor/**}']);
elixir.config.registerWatcher('browserify', ['resources/js/**/*.js', '!resources/js/{vendor,vendor/**}']);

// Gulp tasks for PHP Code Standard Fixer
gulp.task('phpcbf-app', function() {
    return gulp.src(php_files[0])
        .pipe(phpcbf(phpcbf_options))
        .pipe(gulp.dest('app'));
});

gulp.task('phpcbf-config', function() {
    return gulp.src(php_files[1])
        .pipe(phpcbf(phpcbf_options))
        .pipe(gulp.dest('config'));
});

gulp.task('jsbeautify', function() {
    gulp.src(['resources/js/**/*.js', '!resources/js/{vendor,vendor/**}'])
        .pipe(prettify(jsbeautifier_options))
        .pipe(gulp.dest('resources/js/'));
});

// Elixir tasks that are run when invoking <gulp> and <gulp watch>
elixir(function(mix) {
    mix.less('app.less')
        .task('jsbeautify')
        .scripts([
                'vendor/jquery.js',
                'vendor/bootstrap.js'
            ], 'public/js/vendor.js')
        .browserify('app.js')
        .task('phpcbf-app')
        .task('phpcbf-config');
});